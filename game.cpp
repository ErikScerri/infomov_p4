#include "template.h"
#include "ranvec1.cpp"

// global data (source scope)
static Game* game;
static Font font("assets/digital_small.png", "ABCDEFGHIJKLMNOPQRSTUVWXYZ:?!=-0123456789.");
static bool lockState = false;
static Timer stopwatch;
static float duration;

// mountain peaks (push player away)
static float peakx[16] = { 496, 1074, 1390, 1734, 1774, 426, 752, 960, 1366, 1968, 728, 154, 170, 1044, 828, 1712 };
static float peaky[16] = { 398, 446, 166, 748, 1388, 1278, 938, 736, 1090, 290, 126, 82, 784, 570, 894, 704 };
static float peakh[16] = { 400, 300, 320, 510, 400, 510, 400, 600, 240, 200, 160, 160, 160, 320, 320, 320 };

static __m256 peakx8[16] = { 
	_mm256_set1_ps(496),
	_mm256_set1_ps(1074),
	_mm256_set1_ps(1390),
	_mm256_set1_ps(1734),
	_mm256_set1_ps(1774),
	_mm256_set1_ps(426),
	_mm256_set1_ps(752),
	_mm256_set1_ps(960),
	_mm256_set1_ps(1366),
	_mm256_set1_ps(1968),
	_mm256_set1_ps(728),
	_mm256_set1_ps(154),
	_mm256_set1_ps(170),
	_mm256_set1_ps(1044),
	_mm256_set1_ps(828),
	_mm256_set1_ps(1712)
};
static __m256 peaky8[16] = {
	_mm256_set1_ps(398),
	_mm256_set1_ps(446),
	_mm256_set1_ps(166),
	_mm256_set1_ps(748),
	_mm256_set1_ps(1388),
	_mm256_set1_ps(1278),
	_mm256_set1_ps(938),
	_mm256_set1_ps(736),
	_mm256_set1_ps(1090),
	_mm256_set1_ps(290),
	_mm256_set1_ps(126),
	_mm256_set1_ps(82),
	_mm256_set1_ps(784),
	_mm256_set1_ps(570),
	_mm256_set1_ps(894),
	_mm256_set1_ps(704)
};
static __m256 peakh8[16] = {
	_mm256_set1_ps(400),
	_mm256_set1_ps(300),
	_mm256_set1_ps(320),
	_mm256_set1_ps(510),
	_mm256_set1_ps(400),
	_mm256_set1_ps(510),
	_mm256_set1_ps(400),
	_mm256_set1_ps(600),
	_mm256_set1_ps(240),
	_mm256_set1_ps(200),
	_mm256_set1_ps(160),
	_mm256_set1_ps(160),
	_mm256_set1_ps(160),
	_mm256_set1_ps(320),
	_mm256_set1_ps(320),
	_mm256_set1_ps(320)
};

// player, bullet and smoke data
static int aliveP1 = MAXP1, aliveP2 = MAXP2, frame = 0;
//static Bullet bullet[MAXBULLET];
static Bullet bullet;

// dust particle effect tick function
void Particle::Tick()
{
	Pixel* heights = game->heights->GetBuffer();

	parallel_for(int(0), (DUSTPARTICLES / 8), [&](int i)
		//for (int i = 0; i < DUSTPARTICLES / 8; i++)
	{
		//pos += vel;
		posx8[i] = _mm256_add_ps(posx8[i], velx8[i]);
		posy8[i] = _mm256_add_ps(posy8[i], vely8[i]);

		__m256 zero8 = _mm256_setzero_ps();
		__m256 edgex8 = _mm256_set1_ps(2046);
		__m256 edgey8 = _mm256_set1_ps(1534);
		__m256 mask8;

		//(float)((idx * 20261) % 1534)
		union { int rand[8]; __m256i rand8; };
		rand8 = _mm256_mullo_epi32(idx8[i], _mm256_set1_epi32(20261));
		for (int j = 0; j < 8; j++) rand[j] = rand[j] % 1534;

		//if (pos.x < 0) pos.x = 2046, pos.y = (float)((idx * 20261) % 1534); // adhoc rng
		mask8 = _mm256_cmp_ps(posx8[i], zero8, _CMP_LT_OQ);
		posx8[i] = _mm256_blendv_ps(posx8[i], edgex8, mask8);
		posy8[i] = _mm256_blendv_ps(posy8[i], _mm256_cvtepi32_ps(rand8), mask8);

		//if (pos.y < 0) pos.y = 1534;
		mask8 = _mm256_cmp_ps(posy8[i], zero8, _CMP_LT_OQ);
		posy8[i] = _mm256_blendv_ps(posy8[i], edgey8, mask8);

		//if (pos.x > 2046) pos.x = 0;
		mask8 = _mm256_cmp_ps(posx8[i], edgex8, _CMP_GT_OQ);
		posx8[i] = _mm256_blendv_ps(posx8[i], zero8, mask8);

		//if (pos.y > 1534) pos.y = 0;
		mask8 = _mm256_cmp_ps(posy8[i], edgey8, _CMP_GT_OQ);
		posy8[i] = _mm256_blendv_ps(posy8[i], zero8, mask8);

		//-1.0f + vel.x, -0.1f + vel.y
		__m256 normvelx8 = _mm256_add_ps(CONSTNEG1PT0, velx8[i]);
		__m256 normvely8 = _mm256_add_ps(CONSTNEG0PT1, vely8[i]);

		//normalize(float2(-1.0f + vel.x, -0.1f + vel.y))
		__m256 sqnormvelx8 = _mm256_mul_ps(normvelx8, normvelx8);
		__m256 sqnormvely8 = _mm256_mul_ps(normvely8, normvely8);
		__m256 normvel8 = _mm256_rsqrt_ps(_mm256_add_ps(sqnormvelx8, sqnormvely8));

		normvelx8 = _mm256_mul_ps(normvelx8, normvel8);
		normvely8 = _mm256_mul_ps(normvely8, normvel8);

		//float2 force = normalize(float2(-1.0f + vel.x, -0.1f + vel.y)) * speed;
		__m256 forcex8 = _mm256_mul_ps(normvelx8, speedx8[i]);
		__m256 forcey8 = _mm256_mul_ps(normvely8, speedy8[i]);

		//int ix = min( 1022, (int)pos.x / 2 );
		__m256i thresholdx8 = _mm256_set1_epi32(1022);
		__m256i halfposx8 = _mm256_cvtps_epi32(_mm256_div_ps(posx8[i], CONST2PT0));
		__m256i ix8 = _mm256_min_epi32(thresholdx8, halfposx8);

		//int iy = min( 766, (int)pos.y / 2 );
		__m256i thresholdy8 = _mm256_set1_epi32(766);
		__m256i halfposy8 = _mm256_cvtps_epi32(_mm256_div_ps(posy8[i], CONST2PT0));
		__m256i iy8 = _mm256_min_epi32(thresholdy8, halfposy8);

		//ix + iy * 1024
		union { int start[8]; __m256i start8; };
		start8 = _mm256_add_epi32(ix8, _mm256_mullo_epi32(iy8, CONSTINT1024));

		//(ix + 1) + iy * 1024
		union { int nextx[8]; __m256i nextx8; };
		__m256i tmpix8 = _mm256_add_epi32(ix8, _mm256_set1_epi32(1));
		nextx8 = _mm256_add_epi32(tmpix8, _mm256_mullo_epi32(iy8, CONSTINT1024));

		//ix + (iy + 1) * 1024
		union { int nexty[8]; __m256i nexty8; };
		__m256i tmpiy8 = _mm256_add_epi32(iy8, _mm256_set1_epi32(1));
		nexty8 = _mm256_add_epi32(ix8, _mm256_mullo_epi32(tmpiy8, CONSTINT1024));

		//float heightDeltaX = (float)(heights[ix + iy * 1024] & 255) - (heights[(ix + 1) + iy * 1024] & 255);
		//float heightDeltaY = (float)(heights[ix + iy * 1024] & 255) - (heights[ix + (iy + 1) * 1024] & 255);
		union { float heightDeltaX[8]; __m256 heightDeltaX8; };
		union { float heightDeltaY[8]; __m256 heightDeltaY8; };
		for (int j = 0; j < 8; j++)
		{
			heightDeltaX[j] = (float)(heights[start[j]] & 255) - (heights[nextx[j]] & 255);
			heightDeltaY[j] = (float)(heights[start[j]] & 255) - (heights[nexty[j]] & 255);
		}

		//float3 N = normalize( float3( heightDeltaX, heightDeltaY, 38 ) ) * 4.0f;
		__m256 sqnormheightx8 = _mm256_mul_ps(heightDeltaX8, heightDeltaX8);
		__m256 sqnormheighty8 = _mm256_mul_ps(heightDeltaY8, heightDeltaY8);
		__m256 sqnormheightz8 = _mm256_set1_ps(38 * 38);
		__m256 normheight8 = _mm256_rsqrt_ps(_mm256_add_ps(_mm256_add_ps(sqnormheightx8, sqnormheighty8), sqnormheightz8));

		__m256 normheightx8 = _mm256_mul_ps(heightDeltaX8, normheight8);
		__m256 normheighty8 = _mm256_mul_ps(heightDeltaY8, normheight8);

		__m256 Nx8 = _mm256_mul_ps(normheightx8, CONST4PT0);
		__m256 Ny8 = _mm256_mul_ps(normheighty8, CONST4PT0);

		//vel.x = force.x + N.x, vel.y = force.y + N.y;
		velx8[i] = _mm256_add_ps(forcex8, Nx8);
		vely8[i] = _mm256_add_ps(forcey8, Ny8);

		union { int canvaspos[8]; __m256i canvaspos8; };
		__m256i posx8i = _mm256_cvtps_epi32(posx8[i]);
		__m256i posy8i = _mm256_cvtps_epi32(posy8[i]);
		canvaspos8 = _mm256_add_epi32(posx8i, _mm256_mullo_epi32(posy8i, CONSTINT2048));

		for (int j = 0; j < 8; j++)
		{
			Pixel* a = game->canvas->GetBuffer() + canvaspos[j];
			a[0] = AddBlend(a[0], 0x442200), a[2048] = AddBlend(a[2048], 0x442200);
			a[1] = AddBlend(a[1], 0x442200), a[2049] = AddBlend(a[2049], 0x442200);
		}
	});

	//pos += vel;
	//if (pos.x < 0) pos.x = 2046, pos.y = (float)((idx * 20261) % 1534); // adhoc rng
	//if (pos.y < 0) pos.y = 1534;
	//if (pos.x > 2046) pos.x = 0;
	//if (pos.y > 1534) pos.y = 0;
	//float2 force = normalize( float2( -1.0f + vel.x, -0.1f + vel.y ) ) * speed;
	//Pixel* heights = game->heights->GetBuffer();
	//int ix = min( 1022, (int)pos.x / 2 ), iy = min( 766, (int)pos.y / 2 );
	//float heightDeltaX = (float)(heights[ix + iy * 1024] & 255) - (heights[(ix + 1) + iy * 1024] & 255);
	//float heightDeltaY = (float)(heights[ix + iy * 1024] & 255) - (heights[ix + (iy + 1) * 1024] & 255);
	//float3 N = normalize( float3( heightDeltaX, heightDeltaY, 38 ) ) * 4.0f;
	//vel.x = force.x + N.x, vel.y = force.y + N.y;
	//Pixel* a = game->canvas->GetBuffer() + (int)pos.x + (int)pos.y * 2048;
	//a[0] = AddBlend( a[0], 0x442200 ), a[2048] = AddBlend( a[2048], 0x442200 );
	//a[1] = AddBlend( a[1], 0x442200 ), a[2049] = AddBlend( a[2049], 0x442200 );
}

// smoke particle effect tick function
void Smoke::Tick()
{	
	std::atomic_int numchecked = 0;
	parallel_for(int(0), (MAXP1 + MAXP2), [&](int s)
	//for (int s = 0; s < (MAXP1 + MAXP2); s++)
	{
		if (numchecked >= numactive) return;
		if (!active[s]) return;  //.................
		numchecked++;

		bool frameLT64 = (frame[s] < 64);
		int p = frame[s] >> 3;
		if (frameLT64) if (!(frame[s]++ & 7)) puff[s].x[p] = xpos[s], puff[s].y[p] = ypos[s] << 8, puff[s].vy[p] = -450, puff[s].life[p] = 63;
		
		if (p == 0) return; //.................

		__m256i maskFRAME = (frame[s] < 64) ? framelt64 : framegt64;
		__m256i maskFULL = _mm256_and_si256(possiblep[p - 1], _mm256_or_si256(maskFRAME, maskODD));

		puff[s].x8 = _mm256_add_epi32(puff[s].x8, maskFULL);
		puff[s].y8 = _mm256_add_epi32(puff[s].y8, _mm256_mullo_epi32(puff[s].vy8, maskFULL));
		puff[s].vy8 = _mm256_add_epi32(puff[s].vy8, _mm256_mullo_epi32(maskFULL, CONSTINT3));

		__m256i maskGT13 = _mm256_cmpgt_epi32(puff[s].life8, CONSTINT13);
		
		union { int gt[8]; __m256i gt13; };
		gt13 = _mm256_sub_epi32(puff[s].life8, CONSTINT14);
		for (int i = 0; i < 8; i++) gt[i] /= 5;
		gt13 = _mm256_sub_epi32(CONSTINT9, gt13);

		__m256i lt13 = _mm256_srli_epi32(puff[s].life8, 1);

		union { int currframe[8]; __m256i currframe8; };
		currframe8 = _mm256_blendv_epi8(lt13, gt13, maskGT13);
		currframe8 = _mm256_mullo_epi32(currframe8, maskFULL);

		for (unsigned int i = 0; i < p; i++) if (frameLT64 || (i & 1))
		{
			//puff[s].x[i]++, puff[s].y[i] += puff[s].vy[i], puff[s].vy[i] += 3;
			//int currframe = (puff[s].life[i] > 13) ? (9 - (puff[s].life[i] - 14) / 5) : (puff[s].life[i] / 2);
			game->smoke->SetFrame(currframe[i]);
			game->smoke->Draw(puff[s].x[i] - 12, (puff[s].y[i] >> 8) - 12, game->canvas);
			if (!--puff[s].life[i]) puff[s].x[i] = xpos[s], puff[s].y[i] = ypos[s] << 8, puff[s].vy[i] = -450, puff[s].life[i] = 63;
		}


	});

	//int p = frame >> 3;
	//if (frame < 64) if (!(frame++ & 7)) puff[p].x = xpos, puff[p].y = ypos << 8, puff[p].vy = -450, puff[p].life = 63;
	//for (unsigned int i = 0; i < p; i++) if ((frame < 64) || (i & 1))
	//{
	//	puff[i].x++, puff[i].y += puff[i].vy, puff[i].vy += 3;
	//	int frame = (puff[i].life > 13) ? (9 - (puff[i].life - 14) / 5) : (puff[i].life / 2);
	//	game->smoke->SetFrame(frame);
	//	game->smoke->Draw(puff[i].x - 12, (puff[i].y >> 8) - 12, game->canvas);
	//	if (!--puff[i].life) puff[i].x = xpos, puff[i].y = ypos << 8, puff[i].vy = -450, puff[i].life = 63;
	//}
}

// bullet Tick function
void Bullet::TickOld()
{
	for (int i = 0; i < MAXBULLET; i++)
	{
		if (!(flags[i] & Bullet::ACTIVE)) return;
		float2 prevpos;
		prevpos.x = posx[i];
		prevpos.y = posy[i];
		posx[i] += speedx[i] * 1.5f, posy[i] += speedy[i] * 1.5f, prevpos.x -= posx[i] - prevpos.x, prevpos.y -= posy[i] - prevpos.y;
		game->canvas->AddLine(prevpos.x, prevpos.y, posx[i], posy[i], 0x555555);
		if ((posx[i] < 0) || (posx[i] > 2047) || (posy[i] < 0) || (posy[i] > 1535)) flags[i] = 0; // off-screen
		unsigned int start = 0, end = MAXP1;
		if (flags[i] & P1) start = MAXP1, end = MAXP1 + MAXP2;
		/*for (unsigned int j = start; j < end; j++) // check all opponents
		{
		Tank& t = game->tankPrev[j];
		if (!((t.flags & Tank::ACTIVE) && (posx[i] > (t.pos.x - 2)) && (posy[i] > (t.pos.y - 2)) && (posx[i] < (t.pos.x + 2)) && (posy[i] < (t.pos.y + 2)))) continue;
		if (t.flags & Tank::P1) aliveP1--; else aliveP2--;	// update counters
		game->tank[j].flags &= Tank::P1 | Tank::P2;			// kill tank
		flags[i] = 0;											// destroy bullet
		break;
		}*/
	}
}

void Bullet::Tick()
{
	parallel_for(int(0), (MAXBULLET / 8), [&](int bulletsIdx)
	//for (int bulletsIdx = 0; bulletsIdx < MAXBULLET / 8; bulletsIdx++)
	{
		/*if (!(flags & Bullet::ACTIVE))
		{
			return;
		}*/

		if (_mm256_testz_si256(flags8[bulletsIdx], flags8[bulletsIdx])) return; //.................

		//float2 prevpos = pos;
		union { float prevposx[8]; __m256 prevposx8; };
		union { float prevposy[8]; __m256 prevposy8; };
		prevposx8 = posx8[bulletsIdx];
		prevposy8 = posy8[bulletsIdx];

		//pos += speed * 1.5f;
		posx8[bulletsIdx] = _mm256_add_ps(posx8[bulletsIdx], _mm256_mul_ps(speedx8[bulletsIdx], _mm256_set1_ps(1.5f)));
		posy8[bulletsIdx] = _mm256_add_ps(posy8[bulletsIdx], _mm256_mul_ps(speedy8[bulletsIdx], _mm256_set1_ps(1.5f)));

		//prevpos -= pos - prevpos;
		prevposx8 = _mm256_sub_ps(prevposx8, _mm256_sub_ps(posx8[bulletsIdx], prevposx8));
		prevposy8 = _mm256_sub_ps(prevposy8, _mm256_sub_ps(posy8[bulletsIdx], prevposy8));

		for (int i = 0; i < 8; i++)
		{
			game->canvas->AddLine(prevposx[i], prevposy[i], posx[bulletsIdx * 8 + i], posy[bulletsIdx * 8 + i], 0x555555);
		}

		//game->canvas->AddLine8(prevposx8, prevposy8, posx8[bulletsIdx], posy8[bulletsIdx], 0x555555);

		//if ((pos.x < 0) || (pos.x > 2047) || (pos.y < 0) || (pos.y > 1535)) // off-screen
		//{
		//	flags = 0;
		//}
		__m256i ltzXMask = _mm256_cmpgt_epi32(_mm256_cvtps_epi32(posx8[bulletsIdx]), CONSTNEG1);
		__m256i gtmaxXMask = _mm256_cmpgt_epi32(CONST2048, _mm256_cvtps_epi32(posx8[bulletsIdx]));
		__m256i ltzYMask = _mm256_cmpgt_epi32(_mm256_cvtps_epi32(posy8[bulletsIdx]), CONSTNEG1);
		__m256i gtmaxYMask = _mm256_cmpgt_epi32(CONST1535, _mm256_cvtps_epi32(posy8[bulletsIdx]));

		flags8[bulletsIdx] = _mm256_and_si256(flags8[bulletsIdx], ltzXMask);
		flags8[bulletsIdx] = _mm256_and_si256(flags8[bulletsIdx], gtmaxXMask);
		flags8[bulletsIdx] = _mm256_and_si256(flags8[bulletsIdx], ltzYMask);
		flags8[bulletsIdx] = _mm256_and_si256(flags8[bulletsIdx], gtmaxYMask);

		/*unsigned int start = 0;
		unsigned int end = MAXP1;

		if (flags & P1)
		{
			start = MAXP1;
			end = MAXP1 + MAXP2;
		}*/

		union { int start[8]; __m256i start8; };
		union { int end[8]; __m256i end8; };
		start8 = CONST0;
		end8 = _mm256_set1_epi32(MAXP1);

		__m256i player8 = _mm256_and_si256(flags8[bulletsIdx], plyrone8);
		__m256i jumpstart8 = _mm256_set1_epi32(MAXP1 / 2);
		__m256i jumpend8 = _mm256_set1_epi32(MAXP2 / 2);

		start8 = _mm256_add_epi32(start8, _mm256_mullo_epi32(jumpstart8, player8));
		end8 = _mm256_add_epi32(end8, _mm256_mullo_epi32(jumpend8, player8));

		for (int i = 0; i < 8; i++)
		{
			for (unsigned int j = start[i]; j < end[i]; j++) // check all opponents
			{
				Tank& t = *game->tankPrev;

				if (!((t.flags[j] & Tank::ACTIVE) && (posx[bulletsIdx * 8 + i] > (t.posx[j] - 2)) && (posy[bulletsIdx * 8 + i] > (t.posy[j] - 2)) && (posx[bulletsIdx * 8 + i] < (t.posx[j] + 2)) && (posy[bulletsIdx * 8 + i] < (t.posy[j] + 2))))
				{
					continue;
				}

				// update counters
				if (t.flags[j] & Tank::P1)
				{
					aliveP1--;
				}
				else
				{
					aliveP2--;
				}

				game->tank->flags[j] &= Tank::P1 | Tank::P2;			// kill tank
				flags[bulletsIdx * 8 + i] = 0;							// destroy bullet

				break;
			}
		}
	});
}

// Tank::Fire - spawns a bullet
void Tank::Fire(unsigned int party, float2& pos, float2& dir, unsigned int index)
{
	for (unsigned int i = 0; i < MAXBULLET; i++)
	{
		if (!(bullet.flags[i] & Bullet::ACTIVE))
		{
			bullet.flags[i] |= Bullet::ACTIVE + party; // set owner, set active
			bullet.posx[i] = pos.x, bullet.posy[i] = pos.y, bullet.speedx[i] = speedx[index], bullet.speedy[i] = speedy[index];

			return;
		}
	}
}

// Tank::Tick - update single tank
void Tank::Tick()
{
	if (smoke.numactive > 0) smoke.Tick();

	parallel_for(int(0), ((ROUNDUP(MAXP1 + MAXP2, 8)) / 8), [&](int i)
	//for (int i = 0; i < ((ROUNDUP(MAXP1 + MAXP2, 8)) / 8); i++)
	{
		//if (!(flags & ACTIVE)) // dead tank
		union { int status[8]; __m256i status8; };
		status8 = _mm256_and_si256(flags8[i], active8);

		//smoke.xpos = (int)pos.x, smoke.ypos = (int)pos.y;
		for (int j = 0; j < 8; j++)	if ((!status[j]) && (!smoke.active[i * 8 + j]))
		{
			smoke.xpos[i * 8 + j] = (int)posx[i * 8 + j];
			smoke.ypos[i * 8 + j] = (int)posy[i * 8 + j];
			smoke.numactive++;
			smoke.active[i * 8 + j] = true;
		}

		if (_mm256_testz_si256(status8, status8)) return; //.................

		//float2 force = normalize(target - pos);
		__m256 distx8 = _mm256_sub_ps(targetx8[i], posx8[i]);
		__m256 disty8 = _mm256_sub_ps(targety8[i], posy8[i]);

		__m256 sqdistx8 = _mm256_mul_ps(distx8, distx8);
		__m256 sqdisty8 = _mm256_mul_ps(disty8, disty8);

		__m256 normdist8 = _mm256_rsqrt_ps(_mm256_add_ps(sqdistx8, sqdisty8));

		__m256 forcex8 = _mm256_mul_ps(normdist8, distx8);
		__m256 forcey8 = _mm256_mul_ps(normdist8, disty8);

		//evade mountain peaks
		for (int j = 0; j < 16; j++)
		{
			//float2 d(pos.x - peakx[i], pos.y - peaky[i]);
			__m256 dx8 = _mm256_sub_ps(posx8[i], peakx8[j]);
			__m256 dy8 = _mm256_sub_ps(posy8[i], peaky8[j]);

			//float sd = (d.x * d.x + d.y * d.y) * 0.2f;
			__m256 sqdx8 = _mm256_mul_ps(dx8, dx8);
			__m256 sqdy8 = _mm256_mul_ps(dy8, dy8);

			__m256 sd8 = _mm256_mul_ps(sdmodifier8, _mm256_add_ps(sqdx8, sqdy8));

			//if (sd < 1500)
			union { float sdmask[8]; __m256 sdmask8; };
			sdmask8 = _mm256_cmp_ps(sd8, sdcheck8, _CMP_LT_OQ);

			//force += d * 0.03f * (peakh[i] / sd);
			__m256 height8a = _mm256_div_ps(peakh8[j], sd8);
			__m256 height8b = _mm256_mul_ps(height8a, CONST0PT03);
			__m256 sdforcex8 = _mm256_mul_ps(dx8, height8b);
			__m256 sdforcey8 = _mm256_mul_ps(dy8, height8b);

			__m256 tmpforcex8 = _mm256_add_ps(forcex8, sdforcex8);
			__m256 tmpforcey8 = _mm256_add_ps(forcey8, sdforcey8);

			forcex8 = _mm256_blendv_ps(forcex8, tmpforcex8, sdmask8);
			forcey8 = _mm256_blendv_ps(forcey8, tmpforcey8, sdmask8);

			//float r = sqrtf( sd );
			__m256 r8 = _mm256_sqrt_ps(sd8);
			
			for (int curr = 0; curr < 8; curr++)
			{
				if (!sdmask[curr]) continue;
				for (int k = 0; k < 23; k++)
				{
					//float x = peakx[i] + r * fastsin((float)j * PI / 360.0f);
					//float y = peaky[i] + r * fastcos((float)j * PI / 360.0f);
					union { float xA[8]; __m256 xA8; };
					union { float yA[8]; __m256 yA8; };
					union { float xB[8]; __m256 xB8; };
					union { float yB[8]; __m256 yB8; };
					union { float xC[8]; __m256 xC8; };
					union { float yC[8]; __m256 yC8; };
					union { float xD[8]; __m256 xD8; };
					union { float yD[8]; __m256 yD8; };

					union { float kc[8]; __m256 kc8; };
					for (int t = 0; t < 8; t++) kc[t] = t + (8 * k);

					__m256 sin8 = vectorsin(_mm256_mul_ps(kc8, pidiv8));
					__m256 cos8 = vectorcos(_mm256_mul_ps(kc8, pidiv8));

					xA8 = _mm256_add_ps(peakx8[j], _mm256_mul_ps(r8, sin8));
					yA8 = _mm256_add_ps(peaky8[j], _mm256_mul_ps(r8, cos8));

					xB8 = _mm256_add_ps(peakx8[j], _mm256_mul_ps(r8, sin8));
					yB8 = _mm256_sub_ps(peaky8[j], _mm256_mul_ps(r8, cos8));

					xC8 = _mm256_sub_ps(peakx8[j], _mm256_mul_ps(r8, sin8));
					yC8 = _mm256_sub_ps(peaky8[j], _mm256_mul_ps(r8, cos8));

					xD8 = _mm256_sub_ps(peakx8[j], _mm256_mul_ps(r8, sin8));
					yD8 = _mm256_add_ps(peaky8[j], _mm256_mul_ps(r8, cos8));

					//game->canvas->AddPlot((int)x, (int)y, 0x000500);
					for (int t = 0; t < 8; t++) if ((k * 8 + t) < 180)
					{
						game->canvas->AddPlot((int)xA[t], (int)yA[t], 0x000500);
						game->canvas->AddPlot((int)xB[t], (int)yB[t], 0x000500);
						game->canvas->AddPlot((int)xC[t], (int)yC[t], 0x000500);
						game->canvas->AddPlot((int)xD[t], (int)yD[t], 0x000500);
					}
				}
			}

		}

		__m256i collidecur8 = _mm256_setzero_si256();

		//evade other tanks	
		for (int j = 0; j < MAXP1 + MAXP2; j++)
		{
			__m256i samegridx = _mm256_cmpeq_epi32(gridx8[i], _mm256_set1_epi32(game->tankPrev->gridx[j]));
			__m256i samegridy = _mm256_cmpeq_epi32(gridy8[i], _mm256_set1_epi32(game->tankPrev->gridy[j]));

			__m256i samegrid = _mm256_and_si256(samegridx, samegridy);
			if (_mm256_testz_si256(samegridx, samegridy)) continue;

			collidecur8 = _mm256_sub_epi32(collidecur8, samegrid);
			__m256i atmax = _mm256_cmpeq_epi32(collidecur8, collidemax8);
			if (_mm256_testc_si256(atmax, CONSTINTNEG1)) break;

			//if (&game->tank[i] == this) continue;
			int offs = -1;
			if ((j >> 3) == i) { offs = j - (8 * i); }
			union { float offsmask[8]; __m256 offsmask8; };
			offsmask8 = _mm256_cmp_ps(CONST1PT0, CONST1PT0, _CMP_EQ_OQ);
			if (offs >= 0) offsmask[offs] = 0;

			//float2 d = pos - game->tankPrev[i].pos;
			__m256 prevposx8 = _mm256_set1_ps(game->tankPrev->posx[j]);
			__m256 prevposy8 = _mm256_set1_ps(game->tankPrev->posy[j]);

			__m256 dx8 = _mm256_sub_ps(posx8[i], prevposx8);
			__m256 dy8 = _mm256_sub_ps(posy8[i], prevposy8);

			//float len = d.x * d.x + d.y * d.y;
			__m256 sqdx8 = _mm256_mul_ps(dx8, dx8);
			__m256 sqdy8 = _mm256_mul_ps(dy8, dy8);
			__m256 len8 = _mm256_add_ps(sqdx8, sqdy8);

			__m256 maskGT256 = _mm256_and_ps(offsmask8, _mm256_cmp_ps(len8, CONST255PT0, _CMP_LT_OQ));
			if (_mm256_testz_ps(maskGT256, negone8)) continue;

			__m256 maskLT64 = _mm256_and_ps(offsmask8, _mm256_cmp_ps(len8, CONST64PT0, _CMP_LT_OQ));
			//__m256 maskLT256 = _mm256_andnot_ps(maskLT64, _mm256_and_ps(offsmask8, _mm256_cmp_ps(len8, _mm256_set1_ps(256), _CMP_LT_OQ)));
			__m256 maskLT256 = _mm256_andnot_ps(maskLT64, maskGT256);

			//normalize(d)
			__m256 normlen8 = _mm256_rsqrt_ps(len8);
			__m256 normdx8 = _mm256_mul_ps(dx8, normlen8);
			__m256 normdy8 = _mm256_mul_ps(dy8, normlen8);

			__m256 tmpforcex8lt64 = _mm256_add_ps(forcex8, _mm256_mul_ps(normdx8, CONST2PT0));
			__m256 tmpforcey8lt64 = _mm256_add_ps(forcey8, _mm256_mul_ps(normdy8, CONST2PT0));

			__m256 tmpforcex8lt256 = _mm256_add_ps(forcex8, _mm256_mul_ps(normdx8, CONST0PT4));
			__m256 tmpforcey8lt256 = _mm256_add_ps(forcey8, _mm256_mul_ps(normdy8, CONST0PT4));

			//if (len < 64) force += normalize(d) * 2.0f;
			forcex8 = _mm256_blendv_ps(forcex8, tmpforcex8lt64, maskLT64);
			forcey8 = _mm256_blendv_ps(forcey8, tmpforcey8lt64, maskLT64);

			//else if (len < 16 * 16) force += normalize(d) * 0.4f;
			forcex8 = _mm256_blendv_ps(forcex8, tmpforcex8lt256, maskLT256);
			forcey8 = _mm256_blendv_ps(forcey8, tmpforcey8lt256, maskLT256);
		}

		// update speed using accumulated force
		//speed += force;
		speedx8[i] = _mm256_add_ps(speedx8[i], forcex8);
		speedy8[i] = _mm256_add_ps(speedy8[i], forcey8);

		//speed = normalize(speed);
		__m256 sqspeedx8 = _mm256_mul_ps(speedx8[i], speedx8[i]);
		__m256 sqspeedy8 = _mm256_mul_ps(speedy8[i], speedy8[i]);

		__m256 normspeed8 = _mm256_rsqrt_ps(_mm256_add_ps(sqspeedx8, sqspeedy8));

		speedx8[i] = _mm256_mul_ps(speedx8[i], normspeed8);
		speedy8[i] = _mm256_mul_ps(speedy8[i], normspeed8);

		//pos += speed * maxspeed * 0.5f;
		__m256 poschangex8 = _mm256_mul_ps(speedx8[i], _mm256_mul_ps(speedmodifier8, maxspeed8[i]));
		__m256 poschangey8 = _mm256_mul_ps(speedy8[i], _mm256_mul_ps(speedmodifier8, maxspeed8[i]));

		poschangex8 = _mm256_mul_ps(poschangex8, _mm256_cvtepi32_ps(status8));
		poschangey8 = _mm256_mul_ps(poschangey8, _mm256_cvtepi32_ps(status8));

		posx8[i] = _mm256_add_ps(posx8[i], poschangex8);
		posy8[i] = _mm256_add_ps(posy8[i], poschangey8);

		gridx8[i] = _mm256_srli_epi32(_mm256_cvtps_epi32(posx8[i]), 5);
		gridy8[i] = _mm256_srli_epi32(_mm256_cvtps_epi32(posy8[i]), 5);

		// shoot, if reloading completed
		reloading8[i] = _mm256_sub_epi32(reloading8[i], _mm256_set1_epi32(1));

		//union { int allreload[8]; __m256i allreload8; };
		//allreload8 = _mm256_cmpgt_epi32(_mm256_set1_epi32(0), reloading8[i]);
		//if (_mm256_testz_si256(allreload8, allreload8)) continue; //.................

		union { float allreload[8]; __m256 allreload8; };
		allreload8 = _mm256_cmp_ps(_mm256_setzero_ps(), _mm256_cvtepi32_ps(reloading8[i]), _CMP_GT_OQ);
		if (_mm256_testz_ps(allreload8, negone8)) return; //.................

		union { int start[8]; __m256i start8; };
		union { int end[8]; __m256i end8; };

		start8 = _mm256_setzero_si256();
		end8 = _mm256_set1_epi32(MAXP1);

		__m256i player8 = _mm256_and_si256(flags8[i], plyrone8);

		__m256i jumpstart8 = _mm256_set1_epi32(MAXP1 / 2);
		__m256i jumpend8 = _mm256_set1_epi32(MAXP2 / 2);

		start8 = _mm256_add_epi32(start8, _mm256_mullo_epi32(jumpstart8, player8));
		end8 = _mm256_add_epi32(end8, _mm256_mullo_epi32(jumpend8, player8));

		if (start[0] == start[7])
		{
			union { float currspeedx[8]; __m256 currspeedx8; };
			union { float currspeedy[8]; __m256 currspeedy8; };
			union { float currposx[8]; __m256 currposx8; };
			union { float currposy[8]; __m256 currposy8; };

			currspeedx8 = speedx8[i];
			currspeedy8 = speedy8[i];
			currposx8 = posx8[i];
			currposy8 = posy8[i];

			__m256 lencheck = _mm256_set1_ps(100 * 100);
			__m256 dotcheck = _mm256_set1_ps(0.99999f);

			for (unsigned int k = start[0]; k < end[0]; k++) if (game->tankPrev->flags[k] & ACTIVE)
			{
				__m256i samegridx = _mm256_cmpeq_epi32(gridx8[i], _mm256_set1_epi32(game->tankPrev->gridx[k]));
				__m256i samegridy = _mm256_cmpeq_epi32(gridy8[i], _mm256_set1_epi32(game->tankPrev->gridy[k]));

				if (_mm256_testz_si256(samegridx, samegridy)) continue;

				__m256 tpposx8 = _mm256_set1_ps(game->tankPrev->posx[k]);
				__m256 tpposy8 = _mm256_set1_ps(game->tankPrev->posy[k]);

				__m256 dx8 = _mm256_sub_ps(tpposx8, currposx8);
				__m256 dy8 = _mm256_sub_ps(tpposy8, currposy8);

				__m256 sqdx8 = _mm256_mul_ps(dx8, dx8);
				__m256 sqdy8 = _mm256_mul_ps(dy8, dy8);

				__m256 len8 = _mm256_add_ps(sqdx8, sqdy8);

				__m256 normlen8 = _mm256_rsqrt_ps(len8);
				__m256 normdx8 = _mm256_mul_ps(dx8, normlen8);
				__m256 normdy8 = _mm256_mul_ps(dy8, normlen8);

				__m256 dotx8 = _mm256_mul_ps(normdx8, currspeedx8);
				__m256 doty8 = _mm256_mul_ps(normdy8, currspeedy8);
				__m256 dot8 = _mm256_add_ps(dotx8, doty8);

				__m256 maskLT100SQ = _mm256_cmp_ps(len8, lencheck, _CMP_LT_OQ);
				__m256 maskGTDOT99 = _mm256_cmp_ps(dot8, dotcheck, _CMP_GT_OQ);

				union { float maskFULL[8]; __m256 maskFULL8; };
				maskFULL8 = _mm256_and_ps(maskLT100SQ, maskGTDOT99);

				if (!_mm256_testz_ps(maskFULL8, allreload8)) for (int j = 0; j < 8; j++) if (maskFULL[j])
				{
					float2 pos(currposx[j], currposy[j]);
					float2 speed(currspeedx[j], currspeedy[j]);
					Fire(flags[i * 8 + j] & (P1 | P2), pos, speed, i * 8 + j); // shoot
					reloading[i * 8 + j] = 200; // and wait before next shot is ready

					allreload8 = _mm256_cmp_ps(_mm256_setzero_ps(), _mm256_cvtepi32_ps(reloading8[i]), _CMP_GT_OQ);
				}

				if (_mm256_testz_ps(allreload8, negone8)) break;
			}
			return; //.................
		}

		for (int j = 0; j < 8; j++)
		{
			if (allreload[j] == 0) continue;
			for (unsigned int k = start[j]; k < end[j]; k++) if (game->tankPrev->flags[k] & ACTIVE)
			{
				float dx = game->tankPrev->posx[k] - posx[i * 8 + j];
				float dy = game->tankPrev->posy[k] - posy[i * 8 + j];
				float2 d(dx, dy);
				float2 speed(speedx[i * 8 + j], speedy[i * 8 + j]);
				float2 pos(posx[i * 8 + j], posy[i * 8 + j]);

				float len = dx * dx + dy * dy;

				if ((len < (100 * 100)) && (dot(normalize(d), speed) > 0.99999f))
				{
					Fire(flags[i * 8 + j] & (P1 | P2), pos, speed, i * 8 + j); // shoot
					reloading[i * 8 + j] = 200; // and wait before next shot is ready
					break;
				}
			}
		}
	});

	//DONE
	/*for (int i = 0; i < MAXP1 + MAXP2; i++)
	{
	if (--reloading[i] >= 0) continue;
	unsigned int start = 0, end = MAXP1;
	if (flags[i] & P1) start = MAXP1, end = MAXP1 + MAXP2;
	for (unsigned int j = start; j < end; j++) if (game->tankPrev->flags[j] & ACTIVE)
	{
	float dx = game->tankPrev->posx[j] - posx[i];
	float dy = game->tankPrev->posy[j] - posy[i];
	float2 d(dx, dy);
	float2 speed(speedx[i], speedy[i]);
	float2 pos(posx[i], posy[i]);
	if ((length(d) < 100) && (dot(normalize(d), speed) > 0.99999f))
	{
	Fire(flags[i] & (P1 | P2), pos, speed); // shoot
	reloading[i] = 200; // and wait before next shot is ready
	break;
	}
	}
	}*/

	//DONE
	/*if (!(flags & ACTIVE)) // dead tank
	{
	smoke.xpos = (int)pos.x, smoke.ypos = (int)pos.y;
	return smoke.Tick();
	}*/

	//TO ADD SIN/COS
	/*float2 force = normalize( target - pos );
	//evade mountain peaks
	for (unsigned int i = 0; i < 16; i++)
	{
	float2 d( pos.x - peakx[i], pos.y - peaky[i] );
	float sd = (d.x * d.x + d.y * d.y) * 0.2f;
	if (sd < 1500)
	{
	force += d * 0.03f * (peakh[i] / sd);
	float r = sqrtf( sd );
	for (int j = 0; j < 720; j++)
	{
	float x = peakx[i] + r * sinf( (float)j * PI / 360.0f );
	float y = peaky[i] + r * cosf( (float)j * PI / 360.0f );
	float x = peakx[i] + r * fastsin((float)j * PI / 360.0f);
	float y = peaky[i] + r * fastcos((float)j * PI / 360.0f);
	game->canvas->AddPlot( (int)x, (int)y, 0x000500 );
	}
	}
	}*/

	//DONE
	/*// evade other tanks
	for (unsigned int i = 0; i < (MAXP1 + MAXP2); i++)
	{
	if (&game->tank[i] == this) continue;
	float2 d = pos - game->tankPrev[i].pos;
	float len = d.x * d.x + d.y * d.y;
	//if (length( d ) < 8) force += normalize( d ) * 2.0f;
	if (len < 64) force += normalize(d) * 2.0f;
	//else if (length( d ) < 16) force += normalize( d ) * 0.4f;
	else if (len < 16 * 16) force += normalize(d) * 0.4f;
	}*/

	// evade user dragged line	
	/*if ((flags & P1) && (game->leftButton))
	{
	float x1 = (float)game->dragXStart, y1 = (float)game->dragYStart;
	float x2 = (float)game->mousex, y2 = (float)game->mousey;
	float2 N = normalize( float2( y2 - y1, x1 - x2 ) );
	float dist = dot( N, pos ) - dot( N, float2( x1, y1 ) );
	if (fabs( dist ) < 10) if (dist > 0) force += N * 20; else force -= N * 20;
	}*/

	//DONE
	/*// update speed using accumulated force
	speed += force, speed = normalize( speed ), pos += speed * maxspeed * 0.5f;
	// shoot, if reloading completed
	if (--reloading >= 0) return;
	unsigned int start = 0, end = MAXP1;
	if (flags & P1) start = MAXP1, end = MAXP1 + MAXP2;
	for (unsigned int i = start; i < end; i++) if (game->tankPrev[i].flags & ACTIVE)
	{
	float2 d = game->tankPrev[i].pos - pos;
	if ((length( d ) < 100) && (dot( normalize( d ), speed ) > 0.99999f))
	{
	Fire( flags & (P1 | P2), pos, speed ); // shoot
	reloading = 200; // and wait before next shot is ready
	break;
	}
	}*/
}

// Game::Init - Load data, setup playfield
void Game::Init()
{
	// load assets
	backdrop = new Surface("assets/backdrop.png");
	heights = new Surface("assets/heightmap.png");
	canvas = new Surface(2048, 1536); // game runs on a double-sized surface
	p1Sprite = new Sprite(new Surface("assets/p1tank.tga"), 1, Sprite::FLARE);
	p2Sprite = new Sprite(new Surface("assets/p2tank.tga"), 1, Sprite::FLARE);
	pXSprite = new Sprite(new Surface("assets/deadtank.tga"), 1, Sprite::BLACKFLARE);
	smoke = new Sprite(new Surface("assets/smoke.tga"), 10, Sprite::FLARE);

	//tank = new Tank[MAXP1 + MAXP2];
	//tankPrev = new Tank[MAXP1 + MAXP2];
	tank = new Tank();
	tankPrev = new Tank();

	//create tanks
	union { int idx[ROUNDUP(MAXP1 + MAXP2, 8)]; __m256i idx8[(ROUNDUP(MAXP1 + MAXP2, 8)) / 8]; };

	for (unsigned int i = 0; i < MAXP1; i++) tank->flags[i] = Tank::ACTIVE | Tank::P1, idx[i] = i;
	for (unsigned int i = 0; i < MAXP2; i++) tank->flags[i + MAXP1] = Tank::ACTIVE | Tank::P2, idx[i + MAXP1] = i;

	__m256 targetxP1 = _mm256_set1_ps(2048);
	__m256 targetyP1 = _mm256_set1_ps(1536);
	__m256 targetxP2 = _mm256_set1_ps(424);
	__m256 targetyP2 = _mm256_set1_ps(336);
	__m256 maxspeedP1a = _mm256_set1_ps(0.65f);
	__m256 maxspeedP1b = _mm256_set1_ps(0.45f);
	__m256 maxspeedP2 = _mm256_set1_ps(0.3f);

	__m256i p1startx8 = _mm256_set1_epi32(P1STARTX);
	__m256i p1starty8 = _mm256_set1_epi32(P1STARTY);
	__m256i p2startx8 = _mm256_set1_epi32(P2STARTX);
	__m256i p2starty8 = _mm256_set1_epi32(P2STARTY);

	for (uint i = 0; i < ((ROUNDUP(MAXP1 + MAXP2, 8)) / 8); i++)
	{
		// speed & reloading
		tank->speedx8[i] = _mm256_setzero_ps();
		tank->speedy8[i] = _mm256_setzero_ps();
		tank->reloading8[i] = _mm256_setzero_si256();

		__m256i player8 = _mm256_sub_epi32(tank->flags8[i], tank->active8);

		__m256i maskP1 = _mm256_cmpeq_epi32(player8, tank->plyrone8);

		// target
		tank->targetx8[i] = _mm256_blendv_ps(targetxP2, targetxP1, _mm256_cvtepi32_ps(maskP1));
		tank->targety8[i] = _mm256_blendv_ps(targetyP2, targetyP1, _mm256_cvtepi32_ps(maskP1));

		
		union { int P1mod[8]; __m256i P1mod8; };
		union { int P2mod[8]; __m256i P2mod8; };
		union { int P1div[8]; __m256i P1div8; };
		union { int P2div[8]; __m256i P2div8; };

		union { int id[8]; __m256i id8; };
		id8 = idx8[i];

		for (int j = 0; j < 8; j++)
		{
			P1mod[j] = id[j] % 20;
			P1div[j] = id[j] / 20;
			P2mod[j] = id[j] % 32;
			P2div[j] = id[j] / 32;
		}

		// maxspeed
		__m256i maskHALF = _mm256_cmpgt_epi32(_mm256_set1_epi32(MAXP1 / 2), id8);
		
		__m256 maxsp8 = _mm256_blendv_ps(maxspeedP1b, maxspeedP1a, _mm256_cvtepi32_ps(maskHALF));
		tank->maxspeed8[i] = _mm256_blendv_ps(maxspeedP2, maxsp8, _mm256_cvtepi32_ps(maskP1));

		// pos
		__m256i posxtmpP1 = _mm256_add_epi32(_mm256_mullo_epi32(P1mod8, _mm256_set1_epi32(20)), p1startx8);
		__m256i posytmpP1 = _mm256_add_epi32(_mm256_add_epi32(_mm256_mullo_epi32(P1div8, _mm256_set1_epi32(20)), _mm256_set1_epi32(50)), p1starty8);
		__m256i posxtmpP2 = _mm256_add_epi32(_mm256_mullo_epi32(P2mod8, _mm256_set1_epi32(20)), p2startx8);
		__m256i posytmpP2 = _mm256_add_epi32(_mm256_mullo_epi32(P2div8, _mm256_set1_epi32(20)), p2starty8);

		tank->posx8[i] = _mm256_blendv_ps(_mm256_cvtepi32_ps(posxtmpP2), _mm256_cvtepi32_ps(posxtmpP1), _mm256_cvtepi32_ps(maskP1));
		tank->posy8[i] = _mm256_blendv_ps(_mm256_cvtepi32_ps(posytmpP2), _mm256_cvtepi32_ps(posytmpP1), _mm256_cvtepi32_ps(maskP1));

		tank->gridx8[i] = _mm256_srli_epi32(_mm256_cvtps_epi32(tank->posx8[i]), 5);
		tank->gridy8[i] = _mm256_srli_epi32(_mm256_cvtps_epi32(tank->posy8[i]), 5);
	}

	// create blue tanks
	/*for (unsigned int i = 0; i < MAXP1; i++)
	{
		//Tank& t = tank[i];
		//t.pos = float2( (float)((i % 20) * 20) + P1STARTX, (float)((i / 20) * 20 + 50) + P1STARTY );
		//t.target = float2( 2048, 1536 ); // initially move to bottom right corner
		//t.speed = float2( 0, 0 ), t.flags = Tank::ACTIVE | Tank::P1, t.maxspeed = (i < (MAXP1 / 2)) ? 0.65f : 0.45f;

		tank->posx[i] = (float)((i % 20) * 20) + P1STARTX;
		tank->posy[i] = (float)((i / 20) * 20 + 50) + P1STARTY;
		tank->targetx[i] = 2048;
		tank->targety[i] = 1536;
		tank->speedx[i] = 0;
		tank->speedy[i] = 0;
		
		tank->maxspeed[i] = (i < (MAXP1 / 2)) ? 0.65f : 0.45f;
		tank->reloading[i] = 0;
	}*/

	// create red tanks
	/*for (unsigned int i = 0; i < MAXP2; i++)
	{
		//Tank& t = tank[i + MAXP1];
		//t.pos = float2( (float)((i % 32) * 20 + P2STARTX), (float)((i / 32) * 20 + P2STARTY) );
		//t.target = float2( 424, 336 ); // move to player base
		//t.speed = float2( 0, 0 ), t.flags = Tank::ACTIVE | Tank::P2, t.maxspeed = 0.3f;

		tank->posx[i + MAXP1] = (float)((i % 32) * 20 + P2STARTX);
		tank->posy[i + MAXP1] = (float)((i / 32) * 20 + P2STARTY);
		tank->targetx[i + MAXP1] = 424;
		tank->targety[i + MAXP1] = 336;
		tank->speedx[i + MAXP1] = 0;
		tank->speedy[i + MAXP1] = 0;
		
		tank->maxspeed[i + MAXP1] = 0.3f;
		tank->reloading[i] = 0;
	}*/

	// create dust particles
	//particle = new Particle[DUSTPARTICLES];
	particle = new Particle();
	//srand(0);

	//for (int i = 0; i < DUSTPARTICLES; i++)
	for (int i = 0; i < DUSTPARTICLES / 8; i++)
	{
		/*particle[i].pos.x = Rand( 2048 );
		particle[i].pos.y = Rand( 1536 );
		particle[i].idx = i;
		particle[i].speed = Rand( 2 ) + 0.5f;
		particle[i].vel.x = particle[i].vel.y = 0;*/

		Ranvec1 ran(1);
		ran.init(i);

		Vec8f randvecx8 = ran.random8f();
		Vec8f randvecy8 = ran.random8f();
		Vec8f randvecs8 = ran.random8f();
		__m256 randx8 = _mm256_setr_m128(randvecx8.get_low(), randvecx8.get_high());
		__m256 randy8 = _mm256_setr_m128(randvecy8.get_low(), randvecy8.get_high());
		__m256 rands8 = _mm256_setr_m128(randvecs8.get_low(), randvecs8.get_high());

		//particle->posx[i] = Rand(2048);
		//particle->posy[i] = Rand(1536);
		particle->posx8[i] = _mm256_mul_ps(randx8, _mm256_set1_ps(2048));
		particle->posy8[i] = _mm256_mul_ps(randy8, _mm256_set1_ps(1536));

		//particle->idx[i] = i;
		particle->idx8[i] = _mm256_add_epi32(_mm256_mullo_epi32(_mm256_set1_epi32(i), _mm256_set1_epi32(8)), _mm256_set_epi32(7, 6, 5, 4, 3, 2, 1, 0));

		//float speed = Rand(2) + 0.5f;
		//particle->speedx[i] = speed;
		//particle->speedy[i] = speed;
		__m256 speed8 = _mm256_add_ps(_mm256_mul_ps(rands8, _mm256_set1_ps(2)), _mm256_set1_ps(0.5f));
		particle->speedx8[i] = speed8;
		particle->speedy8[i] = speed8;

		//particle->velx[i] = particle->vely[i] = 0;
		particle->velx8[i] = particle->vely8[i] = _mm256_set1_ps(0);
	}

	game = this; // for global reference
	leftButton = prevButton = false;
}

// Game::DrawTanks - draw the tanks
void Game::DrawTanks()
{
	__m256i active8 = _mm256_set1_epi32(Tank::ACTIVE);
	__m256i plyrone8 = _mm256_set1_epi32(Tank::P1);

	__m256 v22 = _mm256_set1_ps(22);
	__m256 v70 = _mm256_set1_ps(70);

	for (int i = 0; i < ((ROUNDUP(MAXP1 + MAXP2, 8)) / 8); i++)
	{
		union { float x[8]; __m256 x8; };
		union { float y[8]; __m256 y8; };
		union { float spx[8]; __m256 spx8; };
		union { float spy[8]; __m256 spy8; };

		x8 = tank->posx8[i];
		y8 = tank->posy8[i];
		spx8 = tank->speedx8[i];
		spy8 = tank->speedy8[i];

		__m256 p1x8 = _mm256_add_ps(x8, _mm256_add_ps(_mm256_mul_ps(v70, spx8), _mm256_mul_ps(v22, spy8)));
		__m256 p1y8 = _mm256_add_ps(y8, _mm256_sub_ps(_mm256_mul_ps(v70, spy8), _mm256_mul_ps(v22, spx8)));

		__m256 p2x8 = _mm256_add_ps(x8, _mm256_sub_ps(_mm256_mul_ps(v70, spx8), _mm256_mul_ps(v22, spy8)));
		__m256 p2y8 = _mm256_add_ps(y8, _mm256_add_ps(_mm256_mul_ps(v70, spy8), _mm256_mul_ps(v22, spx8)));

		union { int maskDEAD[8]; __m256i maskDEAD8; };
		maskDEAD8 = _mm256_and_si256(tank->flags8[i], active8);

		if (!_mm256_testc_si256(maskDEAD8, _mm256_set1_epi32(1))) for (int j = 0; j < 8; j++) if (!maskDEAD[j])
		{
			if (i * 8 + j > MAXP1 + MAXP2) return;

			pXSprite->Draw((int)x[j] - 4, (int)y[j] - 4, canvas);
		}

		if (_mm256_testz_si256(maskDEAD8, _mm256_set1_epi32(1))) continue;

		union { int maskPLAYER[8]; __m256i maskPLAYER8; };
		maskPLAYER8 = _mm256_sub_epi32(tank->flags8[i], active8);

		for (int j = 0; j < 8; j++)
		{
			if (i * 8 + j > MAXP1 + MAXP2) return;

			if (maskPLAYER[j] == Tank::P1)
			{
				p1Sprite->Draw((int)x[j] - 4, (int)y[j] - 4, canvas);
				canvas->Line(x[j], y[j], x[j] + 8 * spx[j], y[j] + 8 * spy[j], 0x4444ff);
			}
			else if (maskPLAYER[j] == Tank::P2)
			{
				p2Sprite->Draw((int)x[j] - 4, (int)y[j] - 4, canvas);
				canvas->Line(x[j], y[j], x[j] + 8 * spx[j], y[j] + 8 * spy[j], 0xff4444);
			}

			// tread marks
			if ((x[j] >= 0) && (x[j] < 2048) && (y[j] >= 0) && (y[j] < 1536))
				backdrop->GetBuffer()[(int)x[j] + (int)y[j] * 2048] = SubBlend(backdrop->GetBuffer()[(int)x[j] + (int)y[j] * 2048], 0x030303);
		}
	}

	/*for (unsigned int i = 0; i < (MAXP1 + MAXP2); i++)
	{
		//Tank& t = tank[i];
		//float x = t.pos.x, y = t.pos.y;
		float x = tank->posx[i], y = tank->posy[i];
		float spx = tank->speedx[i], spy = tank->speedy[i];

		//float2 p1( x + 70 * t.speed.x + 22 * t.speed.y, y + 70 * t.speed.y - 22 * t.speed.x );
		float2 p1(x + 70 * spx + 22 * spy, y + 70 * spy - 22 * spx);
		//float2 p2( x + 70 * t.speed.x - 22 * t.speed.y, y + 70 * t.speed.y + 22 * t.speed.x );
		float2 p2(x + 70 * spx - 22 * spy, y + 70 * spy + 22 * spx);

		//if (!(t.flags & Tank::ACTIVE)) pXSprite->Draw( (int)x - 4, (int)y - 4, canvas ); // draw dead tank
		if (!(tank->flags[i] & Tank::ACTIVE)) pXSprite->Draw((int)x - 4, (int)y - 4, canvas);
		//else if (t.flags & Tank::P1) // draw blue tank
		else if (tank->flags[i] & Tank::P1)
		{
			p1Sprite->Draw((int)x - 4, (int)y - 4, canvas);
			//canvas->Line( x, y, x + 8 * t.speed.x, y + 8 * t.speed.y, 0x4444ff );
			canvas->Line(x, y, x + 8 * spx, y + 8 * spy, 0x4444ff);
		}
		else // draw red tank
		{
			p2Sprite->Draw((int)x - 4, (int)y - 4, canvas);
			//canvas->Line( x, y, x + 8 * t.speed.x, y + 8 * t.speed.y, 0xff4444 );
			canvas->Line(x, y, x + 8 * spx, y + 8 * spy, 0xff4444);
		}
		// tread marks
		if ((x >= 0) && (x < 2048) && (y >= 0) && (y < 1536))
			backdrop->GetBuffer()[(int)x + (int)y * 2048] = SubBlend(backdrop->GetBuffer()[(int)x + (int)y * 2048], 0x030303);
	}*/
}

// Game::PlayerInput - handle player input
void Game::PlayerInput()
{
	if (leftButton) // drag line to guide player tanks
	{
		if (!prevButton) dragXStart = mousex, dragYStart = mousey, dragFrames = 0; // start line
		canvas->ThickLine(dragXStart, dragYStart, mousex, mousey, 0xffffff);
		dragFrames++;
	}
	else // select a new destination for the player tanks
	{
		if ((prevButton) && (dragFrames < 15))
			//for (unsigned int i = 0; i < MAXP1; i++) tank[i].target = float2( (float)mousex, (float)mousey );
			for (unsigned int i = 0; i < MAXP1; i++) tank->targetx[i] = (float)mousex, tank->targety[i] = (float)mousey;
		canvas->Line(0, (float)mousey, 2047, (float)mousey, 0xffffff);
		canvas->Line((float)mousex, 0, (float)mousex, 1535, 0xffffff);
	}
	prevButton = leftButton;
}

// Game::MeasurementStuff: functionality related to final performance measurement
void Game::MeasurementStuff()
{
#ifdef MEASURE
	char buffer[128];
	if (frame & 64) screen->Bar(980, 20, 1010, 50, 0xff0000);
	if (frame >= MAXFRAMES) if (!lockState)
	{
		duration = stopwatch.elapsed();
		lockState = true;
	}
	else frame--;
	if (lockState)
	{
		int ms = (int)duration % 1000, sec = ((int)duration / 1000) % 60, min = ((int)duration / 60000);
		sprintf(buffer, "%02i:%02i:%03i", min, sec, ms);
		font.Centre(screen, buffer, 200);
		sprintf(buffer, "SPEEDUP: %4.1f", REFPERF / duration);
		font.Centre(screen, buffer, 340);
	}
#endif
}

// Game::Tick - main game loop
void Game::Tick(float a_DT)
{
	// get current mouse position relative to full-size bitmap
	POINT p;
#ifdef MEASURE
	p.x = mouseposx[frame];
	p.y = 384;
#else
	GetCursorPos(&p);
	ScreenToClient(FindWindow(NULL, "Template"), &p);
#endif
	leftButton = (GetAsyncKeyState(VK_LBUTTON) != 0), mousex = p.x * 2, mousey = p.y * 2;
	// draw backdrop
	backdrop->CopyTo(canvas, 0, 0);

	// update dust particles
	/*for (int i = 0; i < DUSTPARTICLES; i++)*/
	particle->Tick();

	// mirror current army state, so we can update tanks in any order:
	// always read from previous state, write to current state.
	//memcpy(tankPrev, tank, /*(MAXP1 + MAXP2) **/ sizeof(Tank));
	apex::memcpy(tankPrev, tank, sizeof(Tank));

	// update armies
	/*for (unsigned int i = 0; i < (MAXP1 + MAXP2); i++)*/
	if (!lockState) tank->Tick();

	// update bullets
	/*for (unsigned int i = 0; i < MAXBULLET; i++)*/
	if (!lockState) bullet.Tick();
	// draw armies
	DrawTanks();
	// handle input
#ifndef MEASURE
	PlayerInput();
#endif
	// scale to window size
	canvas->CopyHalfSize(screen);
	// draw lens
	parallel_for(int(p.x - 80), (int)(p.x + 80), [&](int x) /*for (int x = p.x - 80; x < p.x + 80; x++)*/
	{
		for (int y = p.y - 80; y < p.y + 80; y++)
		{
			float sqrttest = (x - p.x) * (x - p.x) + (y - p.y) * (y - p.y);
			if (sqrttest > 6400.0f) continue;
			int rx = mousex + x - p.x, ry = mousey + y - p.y;
			if (rx > 0 && ry > 0 && x > 0 && y > 0 && rx < 2048 && x < 1024 && ry < 1536 && y < 768)
				screen->Plot(x, y, canvas->GetBuffer()[rx + ry * 2048]);
		}
	});
	// report on game state
	MeasurementStuff();
	char buffer[128];
	sprintf(buffer, "FRAME: %04i", frame++);
	font.Print(screen, buffer, 350, 580);
	if ((aliveP1 > 0) && (aliveP2 > 0))
	{
		sprintf(buffer, "blue army: %03i  red army: %03i", aliveP1, aliveP2);
		return screen->Print(buffer, 10, 10, 0xffff00);
	}
	if (aliveP1 == 0)
	{
		sprintf(buffer, "sad, you lose... red left: %i", aliveP2);
		return screen->Print(buffer, 200, 370, 0xffff00);
	}
	sprintf(buffer, "nice, you win! blue left: %i", aliveP1);
	screen->Print(buffer, 200, 370, 0xffff00);
}
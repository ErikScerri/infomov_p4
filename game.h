#ifndef I_GAME_H
#define I_GAME_H

#define ROUNDUP(v,r) (v + (r - 1)) & ~(r - 1)

#define	SCRWIDTH		1024
#define SCRHEIGHT		768

namespace Tmpl8 {

	// final measurement defines
#define MEASURE							// ENABLE THIS FOR FINAL PERFORMANCE MEASUREMENT
#define REFPERF			598413			// record reference performance here//450000

	// modify these values for testing purposes (disable MEASURE)
#define MAXP1			20				// increase to test your optimized code
#define MAXP2			(4 * MAXP1)		// because the player is smarter than the AI
#define MAXBULLET		200
#define P1STARTX		500
#define P1STARTY		300
#define P2STARTX		1300
#define P2STARTY		700
#define DUSTPARTICLES	10000

	// standard values for final measurement, do not modify
#define MAXFRAMES		4000
#ifdef MEASURE
#undef MAXP1
#define MAXP1			750 //750				
#undef MAXP2
#define MAXP2			(4 * MAXP1)
#undef P1STARTX
#define P1STARTX		200
#undef P1STARTY
#define P1STARTY		100
#undef P2STARTX
#define P2STARTX		1300
#undef P2STARTY
#define P2STARTY		700
#undef SCRWIDTH
#define SCRWIDTH		1024
#undef SCRHEIGHT
#define SCRHEIGHT		768
#undef DUSTPARTICLES
#define DUSTPARTICLES	10000
#endif

	class Particle
	{
	public:
		void Tick();
		//float2 pos, vel, speed;
		//int idx;

		const __m256 CONSTNEG1PT0 = _mm256_set1_ps(-1.0f);
		const __m256 CONSTNEG0PT1 = _mm256_set1_ps(-0.1f);
		const __m256 CONST4PT0 = _mm256_set1_ps(4.0f);
		const __m256 CONST2PT0 = _mm256_set1_ps(2);

		const __m256i CONSTINT1024 = _mm256_set1_epi32(1024);
		const __m256i CONSTINT2048 = _mm256_set1_epi32(2048);

		/*union { float posx[DUSTPARTICLES]; __m256 posx8[DUSTPARTICLES / 8]; };
		union { float posy[DUSTPARTICLES]; __m256 posy8[DUSTPARTICLES / 8]; };
		union { float velx[DUSTPARTICLES]; __m256 velx8[DUSTPARTICLES / 8]; };
		union { float vely[DUSTPARTICLES]; __m256 vely8[DUSTPARTICLES / 8]; };
		union { float speedx[DUSTPARTICLES]; __m256 speedx8[DUSTPARTICLES / 8]; };
		union { float speedy[DUSTPARTICLES]; __m256 speedy8[DUSTPARTICLES / 8]; };
		union { int idx[DUSTPARTICLES]; __m256i idx8[DUSTPARTICLES / 8]; };*/

		union { float* posx; __m256* posx8; };
		union { float* posy; __m256* posy8; };
		union { float* velx; __m256* velx8; };
		union { float* vely; __m256* vely8; };
		union { float* speedx; __m256* speedx8; };
		union { float* speedy; __m256* speedy8; };
		union { int* idx; __m256i* idx8; };

		Particle()
		{
			posx = (float*)_aligned_malloc(DUSTPARTICLES * sizeof(float), 64);
			posy = (float*)_aligned_malloc(DUSTPARTICLES * sizeof(float), 64);
			velx = (float*)_aligned_malloc(DUSTPARTICLES * sizeof(float), 64);
			vely = (float*)_aligned_malloc(DUSTPARTICLES * sizeof(float), 64);
			speedx = (float*)_aligned_malloc(DUSTPARTICLES * sizeof(float), 64);
			speedy = (float*)_aligned_malloc(DUSTPARTICLES * sizeof(float), 64);
			idx = (int*)_aligned_malloc(DUSTPARTICLES * sizeof(int), 64);
		}
	};

	/*class Smoke
	{
	public:
		struct Puff { int x, y, vy, life; };
		Smoke() : active(false), frame(0) {};
		void Tick();
		Puff puff[8];
		bool active;
		int frame, xpos, ypos;
	};*/

	class Smoke
	{
	public:
		const __m256i maskODD = _mm256_setr_epi32(0, 1, 0, 1, 0, 1, 0, 1);
		const __m256i framelt64 = _mm256_set1_epi32(1);
		const __m256i framegt64 = _mm256_setzero_si256();

		const __m256i CONSTINT2 = _mm256_set1_epi32(2);
		const __m256i CONSTINT3 = _mm256_set1_epi32(3);
		const __m256i CONSTINT9 = _mm256_set1_epi32(9);
		const __m256i CONSTINT5 = _mm256_set1_epi32(5);
		const __m256i CONSTINT14 = _mm256_set1_epi32(14);
		const __m256i CONSTINT13 = _mm256_set1_epi32(13);

		const __m256i possiblep[8] = {
			_mm256_setr_epi32(1, 0, 0, 0, 0, 0, 0, 0), //frame 1
			_mm256_setr_epi32(1, 1, 0, 0, 0, 0, 0, 0), //frame 2
			_mm256_setr_epi32(1, 1, 1, 0, 0, 0, 0, 0), //frame 3
			_mm256_setr_epi32(1, 1, 1, 1, 0, 0, 0, 0), //frame 4
			_mm256_setr_epi32(1, 1, 1, 1, 1, 0, 0, 0), //frame 5
			_mm256_setr_epi32(1, 1, 1, 1, 1, 1, 0, 0), //frame 6
			_mm256_setr_epi32(1, 1, 1, 1, 1, 1, 1, 0), //frame 7
			_mm256_setr_epi32(1, 1, 1, 1, 1, 1, 1, 1)  //frame 8
		};

		struct Puff
		{
			union { int x[8] = {}; __m256i x8; };
			union { int y[8] = {}; __m256i y8; };
			union { int vy[8] = {}; __m256i vy8; };
			union { int life[8] = {}; __m256i life8; };
		};
		void Tick();
		Smoke() : numactive(0) {};

		int numactive;
		Puff puff[(ROUNDUP(MAXP1 + MAXP2, 8))];
		int active[(ROUNDUP(MAXP1 + MAXP2, 8))] = { false };
		int frame[(ROUNDUP(MAXP1 + MAXP2, 8))] = {};
		int xpos[(ROUNDUP(MAXP1 + MAXP2, 8))];
		int ypos[(ROUNDUP(MAXP1 + MAXP2, 8))];
	};

	/*class Tank
	{
	public:
		enum { ACTIVE = 1, P1 = 2, P2 = 4 };
	
		Tank() : pos( float2( 0, 0 ) ), speed( float2( 0, 0 ) ), target( float2( 0, 0 ) ), reloading( 0 ) {};
		void Fire( unsigned int party, float2& pos, float2& dir );
		void Tick();
	
		float2 pos, speed, target;
		float maxspeed;
		int flags, reloading;
		Smoke smoke;
	};*/

	class Tank
	{
	public:
		enum { ACTIVE = 1, P1 = 2, P2 = 4 };

		const __m256i active8 = _mm256_set1_epi32(ACTIVE);
		const __m256i plyrone8 = _mm256_set1_epi32(P1);
		const __m256 negone8 = _mm256_set1_ps(-1);

		const __m256 sdmodifier8 = _mm256_set1_ps(0.2f);
		const __m256 sdcheck8 = _mm256_set1_ps(1500);
		const __m256 speedmodifier8 = _mm256_set1_ps(0.5f);
		const __m256 pidiv8 = _mm256_set1_ps(PI / 360.0f);

		const __m256 CONST2PT0 = _mm256_set1_ps(2.0f);
		const __m256 CONST1PT0 = _mm256_set1_ps(1.0f);
		const __m256 CONST0PT4 = _mm256_set1_ps(0.4f);
		const __m256 CONST0PT03 = _mm256_set1_ps(0.03f);
		const __m256 CONST255PT0 = _mm256_set1_ps(255);
		const __m256 CONST64PT0 = _mm256_set1_ps(64);

		const __m256i CONSTINT1 = _mm256_set1_epi32(1);
		const __m256i CONSTINTNEG1 = _mm256_set1_epi32(-1);

		const __m256i collidemax8 = _mm256_set1_epi32(8);

		void Fire(unsigned int party, float2& pos, float2& dir, unsigned int index);
		void Tick();

		//union { float posx[MAXP1 + MAXP2]; __m256 posx8[(ROUNDUP(MAXP1 + MAXP2, 8)) / 8]; };
		//union { float posy[MAXP1 + MAXP2]; __m256 posy8[(ROUNDUP(MAXP1 + MAXP2, 8)) / 8]; };
		//union { float speedx[MAXP1 + MAXP2]; __m256 speedx8[(ROUNDUP(MAXP1 + MAXP2, 8)) / 8]; };
		//union { float speedy[MAXP1 + MAXP2]; __m256 speedy8[(ROUNDUP(MAXP1 + MAXP2, 8)) / 8]; };
		//union { float targetx[MAXP1 + MAXP2]; __m256 targetx8[(ROUNDUP(MAXP1 + MAXP2, 8)) / 8]; };
		//union { float targety[MAXP1 + MAXP2]; __m256 targety8[(ROUNDUP(MAXP1 + MAXP2, 8)) / 8]; };
		//union { float maxspeed[MAXP1 + MAXP2]; __m256 maxspeed8[(ROUNDUP(MAXP1 + MAXP2, 8)) / 8]; };
		//union { int flags[MAXP1 + MAXP2]; __m256i flags8[(ROUNDUP(MAXP1 + MAXP2, 8)) / 8]; };
		//union { int reloading[MAXP1 + MAXP2]; __m256i reloading8[(ROUNDUP(MAXP1 + MAXP2, 8)) / 8]; };

		union { float* posx; __m256* posx8; };
		union { float* posy; __m256* posy8; };
		union { float* speedx; __m256* speedx8; };
		union { float* speedy; __m256* speedy8; };
		union { float* targetx; __m256* targetx8; };
		union { float* targety; __m256* targety8; };
		union { float* maxspeed; __m256* maxspeed8; };
		union { int* flags; __m256i* flags8; };
		union { int* reloading; __m256i* reloading8; };
		union { int* gridx; __m256i* gridx8; };
		union { int* gridy; __m256i* gridy8; };

		Smoke smoke;

		Tank()
		{
			posx = (float*)_aligned_malloc((ROUNDUP(MAXP1 + MAXP2, 8)) * sizeof(float), 64);
			posy = (float*)_aligned_malloc((ROUNDUP(MAXP1 + MAXP2, 8)) * sizeof(float), 64);
			speedx = (float*)_aligned_malloc((ROUNDUP(MAXP1 + MAXP2, 8)) * sizeof(float), 64);
			speedy = (float*)_aligned_malloc((ROUNDUP(MAXP1 + MAXP2, 8)) * sizeof(float), 64);
			targetx = (float*)_aligned_malloc((ROUNDUP(MAXP1 + MAXP2, 8)) * sizeof(float), 64);
			targety = (float*)_aligned_malloc((ROUNDUP(MAXP1 + MAXP2, 8)) * sizeof(float), 64);
			maxspeed = (float*)_aligned_malloc((ROUNDUP(MAXP1 + MAXP2, 8)) * sizeof(float), 64);
			flags = (int*)_aligned_malloc((ROUNDUP(MAXP1 + MAXP2, 8)) * sizeof(int), 64);
			reloading = (int*)_aligned_malloc((ROUNDUP(MAXP1 + MAXP2, 8)) * sizeof(int), 64);
			gridx = (int*)_aligned_malloc((ROUNDUP(MAXP1 + MAXP2, 8)) * sizeof(int), 64);
			gridy = (int*)_aligned_malloc((ROUNDUP(MAXP1 + MAXP2, 8)) * sizeof(int), 64);
		}
	};

	class Bullet
	{
	public:
		enum { ACTIVE = 1, P1 = 2, P2 = 4 };
		const __m256i active8 = _mm256_set1_epi32(ACTIVE);
		const __m256i plyrone8 = _mm256_set1_epi32(P1);
		const __m256i CONSTNEG1 = _mm256_set1_epi32(-1);
		const __m256i CONST2048 = _mm256_set1_epi32(2048);
		const __m256i CONST1535 = _mm256_set1_epi32(1535);
		const __m256i CONST0 = _mm256_setzero_si256();

		void TickOld();
		void Tick();

		/*Bullet() : flags( 0 ) {};
		void Tick();
		float2 pos, speed;
		int flags;*/

		union { float posx[MAXBULLET]; __m256 posx8[MAXBULLET / 8]; };
		union { float posy[MAXBULLET]; __m256 posy8[MAXBULLET / 8]; };
		union { float speedx[MAXBULLET]; __m256 speedx8[MAXBULLET / 8]; };
		union { float speedy[MAXBULLET]; __m256 speedy8[MAXBULLET / 8]; };
		union { int flags[MAXBULLET]; __m256i flags8[MAXBULLET / 8]; };

		/*union { float* posx; __m256* posx8; };
		union { float* posy; __m256* posy8; };
		union { float* speedx; __m256* speedx8; };
		union { float* speedy; __m256* speedy8; };
		union { int* flags; __m256i* flags8; };

		Bullet()
		{
			posx = (float*)_aligned_malloc(MAXBULLET * sizeof(float), 64);
			posy = (float*)_aligned_malloc(MAXBULLET * sizeof(float), 64);
			speedx = (float*)_aligned_malloc(MAXBULLET * sizeof(float), 64);
			speedy = (float*)_aligned_malloc(MAXBULLET * sizeof(float), 64);
			flags = (int*)_aligned_malloc(MAXBULLET * sizeof(int), 64);
		}*/
	};

	class Surface;
	class Surface8;
	class Sprite;
	class Game
	{
	public:
		Surface* screen, *canvas, *backdrop, *heights;
		Sprite* p1Sprite, *p2Sprite, *pXSprite, *smoke;
		int mousex, mousey, dragXStart, dragYStart, dragFrames;
		int mouseposx[MAXFRAMES + 1];
		bool leftButton, prevButton;
		Tank* tank, *tankPrev;
		Particle* particle;
		void SetTarget(Surface* s) { screen = s; }
		void SetMousePosX() { for (int i = 0; i < MAXFRAMES + 1; i++) mouseposx[i] = 512 + (int)(400 * sinf/*LUTRS*/((float)i * 0.02f)); }
		void MouseMove(int x, int y) {}
		void MouseButton(bool b) { leftButton = b; }
		void Init();
		void UpdateTanks();
		void UpdateBullets();
		void DrawTanks();
		void PlayerInput();
		void MeasurementStuff();
		void Tick(float a_DT);
	};

}; // namespace Templ8

#endif